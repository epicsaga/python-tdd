# Python TDD Practices

> "[Clean Architectures in Python](https://www.sitepoint.com/premium/books/clean-architectures-in-python)" wrote by Leonardo Giordani
> 1. Test first, code later
> 2. Add the bare minimum amount of code you need to pass the tests
> 3. You shouldn’t have more than one failing test at a time
> 3. Write code that passes the test. Then refactor it.
> 4. A test should fail the first time you run it. If it doesn’t ask yourself why you are adding it.
> 5. Never refactor without tests.

## Calc test

- `pip install -r requirements/dev.txt`
- `py.test -svv tests/test_calc.py`
